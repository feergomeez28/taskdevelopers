package facci.jonathangonzalez.developertasks;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import facci.jonathangonzalez.developertasks.BaseDeDatos.Tarea;

public class AgregarTarea extends AppCompatActivity {

    EditText nombretarea, Descriciontarea;
    Switch EstadoSwicht;
    Button guardartarea;
    TextView estadotarea;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_tarea);
        nombretarea = (EditText)findViewById(R.id.txtnombre);
        Descriciontarea = (EditText)findViewById(R.id.txtDescripcion);
        estadotarea = (TextView)findViewById(R.id.txtEstado);
        guardartarea=(Button)findViewById(R.id.btnguardartarea);
        EstadoSwicht = (Switch)findViewById(R.id.stEstado);

        estadotarea.setText("En desarrollo");

        Intent intent = getIntent();
        final String tituloProyecto = intent.getStringExtra("titulo");

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("Proyecto").child(tituloProyecto).child("Tareas");

        guardartarea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardar();
            }
        });
    }

    private void guardar() {
        String nombre = nombretarea.getText().toString().trim();
        String descripcion = Descriciontarea.getText().toString().trim();
        String estado = estadotarea.getText().toString().trim();

        if (!nombre.isEmpty() && !descripcion.isEmpty()){
            Tarea tarea = new Tarea();
            tarea.setTitulo(nombre);
            tarea.setDescripcion(descripcion);
            tarea.setEstado(estado);
            databaseReference.child(nombre)
                    .setValue(tarea).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Toast.makeText(getApplicationContext(), "Tarea agregada exitosamente", Toast.LENGTH_SHORT).show();
                    pasarPantalla();
                }
            });
        }else if (nombre.isEmpty()){
            nombretarea.setError("Campo Obligatorio");
            nombretarea.requestFocus();
        }else if (descripcion.isEmpty()){
            Descriciontarea.setError("Campo Obligatorio");
            Descriciontarea.requestFocus();
        }

    }

    private void pasarPantalla() {
        Intent intent = new Intent(AgregarTarea.this, TareasActivity.class);
        startActivity(intent);
        this.finish();
    }

    public void switchestado(View view) {
        if (view.getId()==R.id.stEstado){
            if (EstadoSwicht.isChecked()){
                estadotarea.setText("Culminada");
            }else{
                estadotarea.setText("En desarrollo");
            }
        }
    }

}
