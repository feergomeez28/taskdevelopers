package facci.jonathangonzalez.developertasks;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import facci.jonathangonzalez.developertasks.Adaptador.AdaptadorProyectos;
import facci.jonathangonzalez.developertasks.BaseDeDatos.Proyecto;

public class MisProyectosActivity extends AppCompatActivity {

    RecyclerView rv;
    List<Proyecto> lista;
    AdaptadorProyectos adapter;
    FirebaseDatabase db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mis_proyectos);

        rv = (RecyclerView) findViewById(R.id.recyclerProyectos);
        rv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        lista = new ArrayList<>();
        adapter = new AdaptadorProyectos(lista);
        rv.setAdapter(adapter);

        db = FirebaseDatabase.getInstance();

        db.getReference("Proyecto").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Proyecto post = snapshot.getValue(Proyecto.class);
                    lista.add(post);
                }adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MisProyectosActivity.this, TareasActivity.class);
                String tituloProyecto = lista.get(rv.getChildAdapterPosition(v)).getTitulo();
                intent.putExtra("tituloProyecto", tituloProyecto);
                startActivity(intent);
            }
        });

    }
}
