package facci.jonathangonzalez.developertasks;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {

    Button acceder;
    TextView txtNombre, txtContrasena;
    FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        acceder = (Button)findViewById(R.id.buttonacceder);
        txtNombre = (EditText) findViewById(R.id.txtnombre);
        txtContrasena = (EditText) findViewById(R.id.txtcontraseña);
        mAuth = FirebaseAuth.getInstance();



        acceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = txtNombre.getText().toString();
                String password = txtContrasena.getText().toString();

                if (!email.isEmpty() && !password.isEmpty()){
                    mAuth.signInWithEmailAndPassword(email, password)
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(Task<AuthResult> task) {
                                    if (!task.isSuccessful()){
                                        Toast.makeText(getApplicationContext(), "Datos incorrectos", Toast.LENGTH_SHORT).show();
                                    }else {
                                        String correo = mAuth.getCurrentUser().getEmail();
                                        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                                        finish();
                                        Toast.makeText(getApplicationContext(), "Bienvenido " + correo, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                }else if (email.isEmpty()){
                    txtNombre.setError("Campo obligatorio");
                    txtNombre.requestFocus();
                }else if (password.isEmpty()){
                    txtContrasena.setError("Campo obligatorio");
                    txtContrasena.requestFocus();
                }
            }
        });

    }
}
