package facci.jonathangonzalez.developertasks;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import facci.jonathangonzalez.developertasks.BaseDeDatos.Proyecto;

public class NuevoProyectoActivity extends AppCompatActivity {

    Button nuevoproyecto;
    EditText txtNombre, txtDescripcion;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_proyecto);
        txtNombre = (EditText) findViewById(R.id.txtNombreProyecto);
        txtDescripcion = (EditText) findViewById(R.id.txtDescripcion);
        nuevoproyecto=(Button)findViewById(R.id.btnCrearProyecto);

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("Proyecto");

        nuevoproyecto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarProyecto();
            }
        });
    }

    private void guardarProyecto() {
        String titulo = txtNombre.getText().toString().trim();
        String descripcion = txtDescripcion.getText().toString().trim();
        if (!titulo.isEmpty() && !descripcion.isEmpty()){
            Proyecto proyecto = new Proyecto();
            proyecto.setTitulo(titulo);
            proyecto.setDescripcion(descripcion);
            databaseReference.child(titulo)
                    .setValue(proyecto).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Toast.makeText(getApplicationContext(), "Proyecto creado exitosamente", Toast.LENGTH_SHORT).show();
                    pasarPantalla();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(Exception e) {
                    Toast.makeText(getApplicationContext(), "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
                }
            });
        }else if(titulo.isEmpty()) {
            txtNombre.setError("Campo Obligatorio");
            txtNombre.requestFocus();
        }else if (descripcion.isEmpty()){
            txtDescripcion.setError("Campo Obligatorio");
            txtDescripcion.requestFocus();
        }
    }

    private void pasarPantalla() {
        Intent intent = new Intent(NuevoProyectoActivity.this, MisProyectosActivity.class);
        startActivity(intent);
        this.finish();
    }
}
