package facci.jonathangonzalez.developertasks;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import facci.jonathangonzalez.developertasks.Adaptador.AdaptadorProyectos;
import facci.jonathangonzalez.developertasks.Adaptador.AdaptadorTareas;
import facci.jonathangonzalez.developertasks.BaseDeDatos.Proyecto;
import facci.jonathangonzalez.developertasks.BaseDeDatos.Tarea;

public class TareasActivity extends AppCompatActivity {

    RecyclerView rv;
    List<Tarea> lista;
    AdaptadorTareas adapter;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tareas);
        FloatingActionButton fab_add_tarea = (FloatingActionButton) findViewById(R.id.FABAddTarea);

        Intent intent = getIntent();
        final String tituloProyecto = intent.getStringExtra("tituloProyecto");

        rv = (RecyclerView) findViewById(R.id.recyclerTareas);
        rv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        lista = new ArrayList<>();
        adapter = new AdaptadorTareas(lista);
        rv.setAdapter(adapter);

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("Proyecto").child(tituloProyecto).child("Tareas");

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Tarea tarea = snapshot.getValue(Tarea.class);
                    lista.add(tarea);
                }
                Collections.reverse(lista);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        fab_add_tarea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TareasActivity.this, AgregarTarea.class);
                intent.putExtra("titulo", tituloProyecto);
                startActivity(intent);
            }
        });
    }
}
