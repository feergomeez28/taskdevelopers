package facci.jonathangonzalez.developertasks.Adaptador;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import facci.jonathangonzalez.developertasks.BaseDeDatos.Proyecto;
import facci.jonathangonzalez.developertasks.R;

public class AdaptadorProyectos
        extends RecyclerView.Adapter<AdaptadorProyectos.ProyectosViewHolder>
            implements View.OnClickListener{

    List<Proyecto> listProyecto;
    private View.OnClickListener listener;

    public AdaptadorProyectos(List<Proyecto> listProyecto) {
        this.listProyecto = listProyecto;
    }

    @Override
    public ProyectosViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_proyectos, parent, false);
        view.setOnClickListener(this);
        ProyectosViewHolder holder = new ProyectosViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ProyectosViewHolder holder, int position) {
        holder.tvTitulo.setText(listProyecto.get(position).getTitulo());
    }

    @Override
    public int getItemCount() {
        return listProyecto.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }


    @Override
    public void onClick(View v) {
        if (listener != null){
            listener.onClick(v);
        }
    }

    public class ProyectosViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitulo;

        public ProyectosViewHolder(View itemView) {
            super(itemView);
            tvTitulo = (TextView) itemView.findViewById(R.id.lbl_proyecto);
        }
    }
}
