package facci.jonathangonzalez.developertasks.Adaptador;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;
import facci.jonathangonzalez.developertasks.BaseDeDatos.Tarea;
import facci.jonathangonzalez.developertasks.R;


public class AdaptadorTareas
                extends RecyclerView.Adapter<AdaptadorTareas.TareasViewHolder>
                    implements View.OnClickListener {

    List<Tarea> listTarea;
    private View.OnClickListener listener;

    public AdaptadorTareas(List<Tarea> listTarea) {
        this.listTarea = listTarea;
    }

    @Override
    public AdaptadorTareas.TareasViewHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_tareas, parent, false);
        view.setOnClickListener(this);
        AdaptadorTareas.TareasViewHolder holder = new TareasViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(AdaptadorTareas.TareasViewHolder holder, int position) {
        holder.tvTitulo.setText(listTarea.get(position).getTitulo());
        holder.tvDescripcion.setText(listTarea.get(position).getDescripcion());
        holder.tvEstado.setText(listTarea.get(position).getEstado());
    }

    @Override
    public int getItemCount() {
        return listTarea.size();
    }

    public void setOnClickListener(View.OnClickListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if (listener != null){
            listener.onClick(v);
        }
    }

    public class TareasViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitulo, tvDescripcion, tvEstado;
        public TareasViewHolder(View itemView) {
            super(itemView);
            tvTitulo = (TextView) itemView.findViewById(R.id.lbl_titulo_tarea);
            tvDescripcion = (TextView) itemView.findViewById(R.id.lbl_descripcion_tarea);
            tvEstado = (TextView) itemView.findViewById(R.id.lbl_estado_tarea);

        }
    }
}
