package facci.jonathangonzalez.developertasks;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class RegistroActivity extends AppCompatActivity {

    Button crearusuario;
    private FirebaseAuth mAuth;
    TextView txtEmail, txtPassword, txtPaswwordConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        mAuth = FirebaseAuth.getInstance();
        crearusuario =(Button)findViewById(R.id.buttoncrearusuario);
        txtEmail = (EditText) findViewById(R.id.editTextnombreregistro);
        txtPassword = (EditText) findViewById(R.id.editTextcontraseñaregistro);
        txtPaswwordConfirm = (EditText) findViewById(R.id.editTextconfirmarcontraseña);

        crearusuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = txtEmail.getText().toString().trim();
                String password = txtPassword.getText().toString().trim();
                String passwordConfirm = txtPaswwordConfirm.getText().toString().trim();

                if (!email.isEmpty()
                        && !password.isEmpty()
                        && !passwordConfirm.isEmpty()
                        && password.length()>=6
                        && password.equals(passwordConfirm)){

                    mAuth.createUserWithEmailAndPassword(email, password)
                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(Task<AuthResult> task) {
                                    if (task.isSuccessful()){
                                        Toast.makeText(getApplicationContext(), "Usuario ingresado", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(RegistroActivity.this, LoginActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }else {
                                        Toast.makeText(getApplicationContext(), "No se ha podido ingresar usuario", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });

                }else if (email.isEmpty()){
                    txtEmail.setError("Campo obligatorio");
                    txtEmail.requestFocus();
                }else if (password.isEmpty()){
                    txtPassword.setError("Campo obligatorio");
                    txtPassword.requestFocus();
                }else if (passwordConfirm.isEmpty()){
                    txtPaswwordConfirm.setError("Campo obligatorio");
                    txtPaswwordConfirm.requestFocus();
                }else if (password.length()<6){
                    txtPassword.setError("La contraseña debe tener al menos 6 caracteres");
                    txtPassword.requestFocus();
                }else if (passwordConfirm.length()<6){
                    txtPaswwordConfirm.setError("La contraseña debe tener al menos 6 caracteres");
                    txtPaswwordConfirm.requestFocus();
                }else if (!password.equals(passwordConfirm)){
                    txtPaswwordConfirm.setError("Las contraseñas deben coincidir");
                    txtPaswwordConfirm.requestFocus();
                }
            }
        });
    }

}
